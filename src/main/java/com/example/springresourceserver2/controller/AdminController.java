package com.example.springresourceserver2.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

@RestController
@RequestMapping(value = "/admin")
public class AdminController {

    @GetMapping
    public String home() {
        LocalDateTime time = LocalDateTime.now();
        System.out.println("Welcome Home! - " + time + " - ADMIN");
        return "Welcome Home! - " + time + " - ADMIN";
    }
}
