package com.example.springresourceserver2.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

@RestController
@RequestMapping(value = "/user")
public class UserController {

    @GetMapping
    public String home() {
        LocalDateTime time = LocalDateTime.now();
        return "Welcome Home! - " + time + " - USER";
    }
}
