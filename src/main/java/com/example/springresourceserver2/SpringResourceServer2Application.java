package com.example.springresourceserver2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringResourceServer2Application {

    public static void main(String[] args) {
        SpringApplication.run(SpringResourceServer2Application.class, args);
    }

}
